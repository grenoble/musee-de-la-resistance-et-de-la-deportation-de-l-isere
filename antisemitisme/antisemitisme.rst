

.. _mrdi_antisemitisme:

==================================================================================
Antisémitisme
==================================================================================

- :ref:`antisem:antisem`

Définition de l'antisémitisme
===============================

.. figure:: 1_definition_antisemitisme.webp  

Lettre de M. L. Catiglioni
===============================

.. figure:: 20231208_145748_800.webp  

Etre Juif en Isère dans les années 1930
==============================================

.. figure:: 20231208_150519_800.webp  

Sous l'occupation italienne
===============================

.. figure:: 20231208_150748_800.webp  


De novembre 1942 à spetmbre 1943 l'armée italienne occupe Grenoble
====================================================================

.. figure:: 20231208_145842_800.webp  

What it means to be Jewish is Isère in the 1930s
=====================================================

.. figure:: 20231208_150525_800.webp  


Courrier intercepté par la censure allemande (la vraie Palestine)
=====================================================================

.. figure:: 20231208_150913_800.webp  

Grenoble ... car ici c'est la vraie Palestine
==================================================

.. figure:: 20231208_150856_800.webp  

Les Justes
===============================

.. figure:: 20231208_151650_800.webp  

Statut des Juifs le 3 octobre 1940
===========================================

.. figure:: 20231208_144217_800.webp         



Origine des réfugiés juifs en Isère
=======================================

.. figure:: 20231208_150559_800.webp  


Sur les 10 à 30 mille Juifs présents en Isère, la plupart échappèrent à l'arrestation et à la déportattion
===============================================================================================================

.. figure:: 20231208_151728_800.webp  

Carte d'identité Juive
===============================

.. figure:: 20231208_144253_800.webp         


Entreprise Juive
===============================

.. figure:: 20231208_150323_800.webp  


Consignes pour la rafle du 26 août 1942
=============================================

.. figure:: 20231208_150706_800.webp  

**La rafle du 26 août 1942**
===============================

.. figure:: 20231208_150630_800.webp  

**La rafle du 26 août 1942** (en anglais)
==============================================

.. figure:: 20231208_150636_800.webp  

Carte de la rafle du 26 août 1942
====================================

.. figure:: 20231208_150647_800.webp  


**Arrivée des Allemands en septembre 1943 arrestation massive de Juifs en 1944**
====================================================================================

.. figure:: 20231208_151038_800.webp  


Arrestation des Juifs de septembre 1943 à août 1944
========================================================

.. figure:: 20231208_151121_800.webp  


Les Justes de l'Isère en 2008
===============================

.. figure:: 20231208_151750_800.webp

L'entrée des Juifs sans les salles de l'hôtel des ventes est interdite de'une manière absolue
===================================================================================================

.. figure:: 20231208_144345_800.webp         


Intérieurs des ateliers des Gants Fischler
===============================================

.. figure:: 20231208_150337_800.webp  



**Il faut attendre 1973 pour que commence à s'exprimer la mémoire des Juifs déportés** (grâce à Simone Lagrange)
====================================================================================================================

.. figure:: 20231208_151907_800.webp


Les Juifs, nos frères (Combat N°35, octobre 1942)
=======================================================

- https://fr.wikipedia.org/wiki/Albert_Camus (rédacteur pour le journal Combat)
- :ref:`combat_1941`

.. figure:: 20231208_145458_800.webp  

Fausses cartes d'identité
===============================

.. figure:: 20231208_150412_800.webp  


.. _portrait_des_enfants_deportes:

**Portrait d'enfants déportés (Grenoble)**
============================================

- :ref:`antisem:memorial_des_enfants`

.. figure:: 20231208_151611_800.webp  


**Sur le millier de Juifs déportés depuis l'Isère, 80 sont des enfants, 3 seulement surivivront**
====================================================================================================

- :ref:`antisem:memorial_des_enfants`

.. figure:: 20231208_151534_800.webp  

**Les enfants arrêtés et déportés de l'Isère**
===============================================

- :ref:`antisem:memorial_des_enfants`

.. figure:: 20231208_151602_800.webp  


Les Juifs nos frères
===============================

.. figure:: 20231208_145506_800.webp  

Les spoliations (loi du 22 juillet 1941)
============================================

.. figure:: 20231208_150441_800.webp  



