﻿#!/usr/bin/env bash
# The image size_max will be 800

size_max=800
mkdir ${size_max}

for filename in *.jpg; do
    input=${filename}
    name=${filename%.*}
    suffixe='webp'
    output="800/"${name}.${suffixe}
    echo "Input: ${input}"
    echo "File Name: ${name}"
    echo "File Extension: ${suffixe}"
    echo "output: ${output}"

    convert ${input} -resize ${size_max} ${output}
done
