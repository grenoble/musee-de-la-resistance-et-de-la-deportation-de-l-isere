

.. raw:: html

   <a rel="me" href="https://kolektiva.social/@raar"></a>
   <a rel="me" href="https://piaille.fr/@raar"></a>
   <a rel="me" href="https://framapiaf.org/@goldman_bakounine"></a>
   <a rel="me" href="https://framapiaf.org/@pvergain"></a>


.. un·e
.. https://framapiaf.org/web/tags/raar.rss
.. https://framapiaf.org/web/tags/golem.rss
.. https://framapiaf.org/web/tags/racisme.rss
.. https://framapiaf.org/web/tags/racism.rss
.. https://nitter.cz/


.. _mrdi:
.. _resistance_gre:

==================================================================================
**Musée de la Résistance et de la Déportation de l'Isère** 
==================================================================================

- https://musees.isere.fr/page/musee-de-la-resistance-et-de-la-deportation-de-lisere-le-musee
- https://www.youtube.com/@mrdi-mdh6818/videos
- https://fr.wikipedia.org/wiki/Mus%C3%A9e_de_la_R%C3%A9sistance_et_de_la_D%C3%A9portation_de_l%27Is%C3%A8re


- https://www.instagram.com/culture.isere/
- https://www.youtube.com/user/isereculture
- https://archives.isere.fr/
- https://musees.isere.fr/
- https://www.youtube.com/@mediathequedepartementaled9474/videos
- https://archives.isere.fr/page/qui-sommes-nous
- https://cinevod.bm-grenoble.fr/videos/category/9RIOF-fonds-cinpress-jack-lesage



ADRESSE ET CONTACT
=======================

::

	Musée de la Résistance et de la Déportation de l'Isère
	14, rue Hébert
	38000 Grenoble
	+33 (0)4 76 42 38 53

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.734482407569886%2C45.189643993783726%2C5.7380229234695435%2C45.191180803357554&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.19041/5.73625">Afficher une carte plus grande</a></small>
   

JOURS ET HORAIRES D'OUVERTURE
--------------------------------


- Lundi-vendredi : 9h / 18h
- Mardi : 13h30 / 18h
- Samedi, dimanche et jours fériés : 10h / 18h




.. toctree::
   :maxdepth: 3

   a-propos/a-propos
   antisemitisme/antisemitisme
   communistes/communistes   
   macons/macons
   resistance/resistance
