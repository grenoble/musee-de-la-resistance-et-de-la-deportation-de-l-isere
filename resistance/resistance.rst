

.. _mrdi_resistance:

==================================================================================
Résistance
==================================================================================

- https://fr.wikipedia.org/wiki/R%C3%A9sistance_fran%C3%A7aise
- :ref:`antisem:antisem`


Dès 1940, l'Isère entre en résistance
===========================================

.. figure:: 20231208_144518_800.webp         


.. _combat_1941:

**Naissance du mouvement Combat avec Marie Reynoard en novembre 1941 (4 rue Joseph Fourier)**
==================================================================================================

- https://fr.wikipedia.org/wiki/Combat_(R%C3%A9sistance)
- https://fr.wikipedia.org/wiki/Marie_Reynoard
- https://fr.wikipedia.org/wiki/Albert_Camus (rédacteur pour le journal Combat)

.. figure:: 20231208_144622_800.webp         

.. raw:: html

   <iframe width="800" height="400" src="https://www.openstreetmap.org/export/embed.html?bbox=5.732958912849427%2C45.18852302286745%2C5.736499428749085%2C45.190059862708196&amp;layer=mapnik" style="border: 1px solid black">
   </iframe><br/><small><a href="https://www.openstreetmap.org/#map=19/45.18929/5.73473">Afficher une carte plus grande</a></small>
   

Doyen Gosse, abbé Pierre
===============================

.. figure:: 20231208_144955_800.webp  


Doyen Gosse, abbé Pierre en anglais
=====================================

.. figure:: 20231208_145004_800.webp  


Grenoble ... capitale des maquis
=====================================

.. figure:: 20231208_150025_800.webp  


Résistants SFIO
===============================

.. figure:: 20231208_145202_800.webp  


Réunion à Méaudre le 25 janvier 1944, nom de code Monaco
=============================================================

.. figure:: 20231208_152031_800.webp

