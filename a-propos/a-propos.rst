

.. _a_propos:

==================================================================================
**A propos**
==================================================================================

Le musée en quelques mots 
============================

Initié il y a plus de cinquante ans par d’anciens résistants, déportés et 
des enseignants, conçu dans un esprit pédagogique et de transmission, 
le Musée de la Résistance et de la Déportation de l’Isère est un musée 
d’histoire et de société. 

**En 1994, il devient départemental et s’installe 14, rue Hébert à Grenoble**. 

Il met en lumière l’histoire de la Seconde Guerre mondiale à partir des 
faits et vécus locaux et restitue dans leur chronologie, les causes et 
les conséquences du conflit. 

Il permet aussi de comprendre comment et à partir de quels choix individuels 
est née la Résistance et souligne l’ampleur des souffrances et des sacrifices 
de ceux qui se sont engagés pour permettre le retour de la République. 

**Au-delà, le musée interroge le visiteur sur les enseignements que notre 
société peut tirer de l’histoire, autour des valeurs intemporelles de la 
Résistance et celles des Droits de l’Homme**.

